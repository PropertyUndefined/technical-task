import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import PublicRoute from "./router/PublicRoute";
import PrivateRoute from "./router/PrivateRoute";
import HomePage from "./components/HomePage";
import UsersPage from "./components/UsersPage";
import UserInfoPage from "./components/UserInfoPage";

function App() {
  return (
    <Router>
      <PublicRoute path="/" exact>
        <HomePage />
      </PublicRoute>
      <PrivateRoute path="/users">
        <UsersPage />
      </PrivateRoute>
      <PrivateRoute path="/user/:userId">
        <UserInfoPage />
      </PrivateRoute>
    </Router>
  );
}

export default App;
