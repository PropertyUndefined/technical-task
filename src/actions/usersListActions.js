import * as actionTypes from "./actionTypes";

export const fetchUsersRequest = () => {
  return {
    type: actionTypes.FETCH_USERS_REQUEST,
  };
};

export const fetchUsersSuccess = (users) => {
  return {
    type: actionTypes.FETCH_USERS_SUCCESS,
    payload: users,
  };
};

export const fetchUsersError = () => {
  return {
    type: actionTypes.FETCH_USERS_ERROR,
  };
};

export const changeFilter = (payload) => {
  return {
    type: actionTypes.CHANGE_FILTER,
    payload: payload,
  };
};

export const toggleSort = (payload) => {
  return {
    type: actionTypes.TOGGLE_SORT,
    payload: payload,
  };
};
