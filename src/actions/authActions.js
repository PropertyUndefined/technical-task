import * as actionTypes from "./actionTypes";

export const changeStatus = () => {
  return {
    type: actionTypes.CHANGE_STATUS,
  };
};
