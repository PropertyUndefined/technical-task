export const getRandomUsers = async () => {
  const response = await fetch("https://randomuser.me/api/?results=50");
  const users = response.json();
  return users;
};
