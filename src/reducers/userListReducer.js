import createReducer from "../helpers/ReduxUtils";
import * as actionTypes from "../actions/actionTypes";

const initialState = {
  users: [],
  isLoading: false,
  hasError: false,
  errorMessage: "",
  filter: "",
  sortBy: "Full name",
  sortIsAscending: true,
};

export const userListReducer = createReducer(initialState, {
  [actionTypes.FETCH_USERS_REQUEST]: (state) => {
    return { ...state, isLoading: true };
  },
  [actionTypes.FETCH_USERS_SUCCESS]: (state, { payload }) => {
    return { ...state, users: payload, isLoading: false };
  },
  [actionTypes.FETCH_USERS_ERROR]: (state) => {
    return {
      ...state,
      isLoading: false,
      hasError: true,
    };
  },
  [actionTypes.CHANGE_FILTER]: (state, { payload }) => {
    return {
      ...state,
      filter: payload,
    };
  },
  [actionTypes.TOGGLE_SORT]: (state, { payload }) => {
    let isAscending;
    if (payload === state.sortBy) {
      isAscending = !state.sortIsAscending;
    } else {
      isAscending = true;
    }

    return { ...state, sortBy: payload, sortIsAscending: isAscending };
  },
});
