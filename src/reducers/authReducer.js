import createReducer from "../helpers/ReduxUtils";
import * as actionTypes from "../actions/actionTypes";

const initialState = {
  isAuthenticated: JSON.parse(window.localStorage.getItem("isAuthenticated")),
};

export const authReducer = createReducer(initialState, {
  [actionTypes.CHANGE_STATUS]: (state) => {
    window.localStorage.setItem("isAuthenticated", `${!state.isAuthenticated}`);
    return { ...state, isAuthenticated: !state.isAuthenticated };
  },
});
