import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { userListReducer } from "./userListReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  userList: userListReducer,
});

export default rootReducer;
