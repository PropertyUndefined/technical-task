const userCardCategoryTypes = {
  ADDRESS: "ADDRESS",
  COORDINATES: "COORDINATES",
  TIME_ZONE: "TIME_ZONE",
  AGE: "AGE",
  DATE: "DATE",
  DEFAULT: "DEFAULT",
};

export default userCardCategoryTypes;
