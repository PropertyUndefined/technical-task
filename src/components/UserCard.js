import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import UserCardInfoCategory from "./UserCardInfoCategory";
import userCardCategoryTypes from "../types/userCardCategoryTypes";

const UserCard = ({ user }) => {
  return (
    <div className="user-card">
      <div className="user-card__content">
        {!user ? (
          <h2>Sorry, the respective user has not been found : ( </h2>
        ) : (
          <>
            <div className="user-card__image">
              <img src={user?.picture?.large} alt="hero of the card" />
            </div>
            <h2>{`${user?.name?.title} ${user?.name?.first} ${user?.name?.last} `}</h2>
            <div className="user-card__block-list">
              <UserCardInfoCategory name="Gender" info={user?.gender} />
              <UserCardInfoCategory
                name="Address"
                type={userCardCategoryTypes.ADDRESS}
                info={[
                  user?.location?.street?.number,
                  user?.location?.street?.name,
                  user?.location?.city,
                  user?.location?.state,
                  user?.location?.country,
                  user?.location?.postcode,
                ]}
              />
              <UserCardInfoCategory
                name="Coordinates"
                type={userCardCategoryTypes.COORDINATES}
                info={[
                  user?.location?.coordinates?.latitude,
                  user?.location?.coordinates?.longitude,
                ]}
              />
              <UserCardInfoCategory
                name="Time zone"
                type={userCardCategoryTypes.TIME_ZONE}
                info={[
                  user?.location?.timezone?.offset,
                  user?.location?.timezone?.description,
                ]}
              />
              <UserCardInfoCategory name="E-mail" info={user?.email} />
              <UserCardInfoCategory name="User ID" info={user?.login?.uuid} />
              <UserCardInfoCategory
                name="Username"
                type={userCardCategoryTypes.DEFAULT}
                info={user?.login?.username}
              />
              <UserCardInfoCategory
                name="Password"
                info={user?.login?.password}
              />
              <UserCardInfoCategory
                name="Date of birth"
                type={userCardCategoryTypes.DATE}
                info={user?.dob?.date}
              />
              <UserCardInfoCategory
                name="Age"
                type={userCardCategoryTypes.AGE}
                info={user?.dob?.age}
              />
              <UserCardInfoCategory
                name="Date of registration"
                type={userCardCategoryTypes.DATE}
                info={user?.registered?.date}
              />
              <UserCardInfoCategory
                name="Has been a member for"
                type={userCardCategoryTypes.AGE}
                info={user?.registered?.age}
              />
              <UserCardInfoCategory name="Phone number" info={user?.phone} />
              <UserCardInfoCategory
                name="Cell phone number"
                info={user?.cell}
              />
              <UserCardInfoCategory name="Nationality" info={user?.nat} />
            </div>
          </>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (
  state,
  {
    match: {
      params: { userId },
    },
  }
) => {
  const user = state.userList.users.find((user) => user.login.uuid === userId);
  return {
    user,
  };
};

export default withRouter(connect(mapStateToProps, null)(UserCard));
