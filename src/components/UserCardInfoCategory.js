import React from "react";
import userCardCategoryTypes from "../types/userCardCategoryTypes";
import formatDate from "../helpers/DateTimeUtils";

const mapCategoryTypeToPrinter = {
  [userCardCategoryTypes.ADDRESS]: (info) => {
    const address = info.filter((data) => data);
    return address.join(", ");
  },
  [userCardCategoryTypes.COORDINATES]: (info) => {
    const [latitude, longitude] = info;
    return `${latitude ? `Latitude ${latitude} & ` : ""}${
      longitude ? `Longitude ${longitude}` : ""
    }`;
  },
  [userCardCategoryTypes.TIME_ZONE]: (info) => {
    const [offset, description] = info;
    return `${offset ? `${offset} GMT: ` : ""}${
      description ? description : ""
    }`;
  },
  [userCardCategoryTypes.AGE]: (age) => {
    return age ? `${age} years` : "";
  },
  [userCardCategoryTypes.DATE]: (date) => {
    return date ? formatDate(new Date(date)) : "";
  },
  [userCardCategoryTypes.DEFAULT]: (info) => {
    return info ? info : "";
  },
};

const UserCardInfoCategory = ({ info, name, type }) => {
  return (
    <div className="user-card__content-block">
      <div className="user-card__content-category">{name}:</div>
      <div className="user-card__content-info">
        {type
          ? mapCategoryTypeToPrinter[type](info)
          : mapCategoryTypeToPrinter[userCardCategoryTypes.DEFAULT](info)}
      </div>
    </div>
  );
};

export default UserCardInfoCategory;
