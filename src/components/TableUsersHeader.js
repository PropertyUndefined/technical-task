import React from "react";
import { connect } from "react-redux";
import { toggleSort } from "../actions/usersListActions";
import Emoji from "./Emoji";

const TableUsersHeader = ({
  name,
  sortable,
  sortBy,
  sortIsAscending,
  toggleSort,
}) => {
  const handleClick = (event) => {
    toggleSort(event.currentTarget.getAttribute("data-name"));
  };

  return (
    <th
      className={`users-table__head ${sortable ? "with-pointer" : "no-events"}`}
      onClick={handleClick}
      data-name={name}
    >
      <span>{name}</span>
      {sortable ? (
        name === sortBy ? (
          sortIsAscending ? (
            <Emoji label="Up arrow" symbol="⬆️" />
          ) : (
            <Emoji label="Down arrow" symbol="⬇️" />
          )
        ) : (
          <Emoji label="Up - down arrows" symbol="↕️" />
        )
      ) : (
        "️"
      )}
    </th>
  );
};

const mapStateToProps = (state) => {
  return {
    sortBy: state.userList.sortBy,
    sortIsAscending: state.userList.sortIsAscending,
  };
};

export default connect(mapStateToProps, { toggleSort })(TableUsersHeader);
