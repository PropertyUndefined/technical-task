import React from "react";
import { connect } from "react-redux";
import { changeFilter } from "../actions/usersListActions";

const Filter = ({ changeFilter, filter }) => {
  const handleChange = (event) => {
    changeFilter(event.target.value);
  };

  return (
    <div className="filter">
      <label htmlFor="filter">Filter by surname:</label>
      <input
        onChange={handleChange}
        name="filter"
        type="text"
        placeholder="Start typing"
        value={filter}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    filter: state.userList.filter,
  };
};

export default connect(mapStateToProps, { changeFilter })(Filter);
