import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import StatusButton from "./StatusButton";

const HomePage = ({ isAuth }) => {
  if (isAuth) {
    return <Redirect to="/users" />;
  } else {
    return (
      <div className="home-container">
        <h1>Well met! You definitely need to log in to proceed.</h1>
        <StatusButton />
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.isAuthenticated,
  };
};

export default connect(mapStateToProps, null)(HomePage);
