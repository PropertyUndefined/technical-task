import React, { useEffect } from "react";
import StatusButton from "./StatusButton";
import UsersList from "./TableUsers";
import Filter from "./Filter";
import { connect } from "react-redux";
import { getRandomUsers } from "../services/randomUserServiceProvider";
import {
  fetchUsersError,
  fetchUsersRequest,
  fetchUsersSuccess,
} from "../actions/usersListActions";

const UsersPage = ({
  fetchUsersRequest,
  fetchUsersSuccess,
  fetchUsersError,
}) => {
  useEffect(() => {
    async function fetchUsers() {
      fetchUsersRequest();

      try {
        const data = await getRandomUsers();
        fetchUsersSuccess(data.results);
      } catch (error) {
        fetchUsersError();
      }
    }
    fetchUsers();
  }, [fetchUsersRequest, fetchUsersSuccess, fetchUsersError]);

  return (
    <div>
      <StatusButton isSmall={true} />
      <Filter />
      <UsersList />
    </div>
  );
};

export default connect(null, {
  fetchUsersRequest,
  fetchUsersSuccess,
  fetchUsersError,
})(UsersPage);
