import React from "react";
import TableUserRow from "./TableUserRow";
import TableUsersHeader from "./TableUsersHeader";
import { connect } from "react-redux";
import { ascendingAgeSort, ascendingSurnameSort } from "../helpers/SortUtils";

const TableUsers = ({
  users,
  isLoading,
  hasError,
  filter,
  sortIsAscending,
  sortBy,
}) => {
  const mapColumnToSorter = {
    "Full name": (userOne, userTwo) =>
      ascendingSurnameSort(userOne, userTwo, sortIsAscending),
    "Date of birth": (userOne, userTwo) =>
      ascendingAgeSort(userOne, userTwo, sortIsAscending),
  };
  const usersToRender = users
    .filter((user) => {
      return user.name.last.toLowerCase().includes(filter.toLowerCase());
    })
    .sort(mapColumnToSorter[sortBy]);
  return (
    <>
      <div className="users-table-container">
        <div className="users-table__head">
          <table cellPadding="0" cellSpacing="0" border="0">
            <thead>
              <tr>
                <TableUsersHeader name="Full name" sortable />
                <TableUsersHeader name="Photo" />
                <TableUsersHeader name="E-mail" />
                <TableUsersHeader name="Date of birth" sortable />
                <TableUsersHeader name="Get more info" />
              </tr>
            </thead>
          </table>
        </div>
        {isLoading && <h2>The users are loading...</h2>}
        {hasError && (
          <h2>Sorry, an error has occurred, try to reload the page.</h2>
        )}
        <div className="users-table__content">
          <table className="users-table">
            <tbody>
              {usersToRender.map((user) => {
                return <TableUserRow key={user.login.uuid} user={user} />;
              })}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export const mapStateToProps = (state) => {
  return {
    hasError: state.userList.hasError,
    isLoading: state.userList.isLoading,
    users: state.userList.users,
    filter: state.userList.filter,
    sortBy: state.userList.sortBy,
    sortIsAscending: state.userList.sortIsAscending,
  };
};

export default connect(mapStateToProps, null)(TableUsers);
