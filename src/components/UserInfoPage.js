import React from "react";
import StatusButton from "./StatusButton";
import UserCard from "./UserCard";

export const UserInfoPage = () => {
  return (
    <div>
      <StatusButton isSmall={true} />
      <UserCard />
    </div>
  );
};

export default UserInfoPage;
