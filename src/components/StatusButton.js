import React from "react";
import { connect } from "react-redux";
import { changeStatus } from "../actions/authActions";

const StatusButton = ({ isAuthenticated, changeStatus, isSmall }) => {
  const handleClick = () => {
    changeStatus();
  };

  return (
    <button
      className={`btn ${isSmall ? "btn--small" : ""}`}
      onClick={handleClick}
    >
      {isAuthenticated ? "Log out" : "Log in"}
    </button>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

export default connect(mapStateToProps, { changeStatus })(StatusButton);
