import React from "react";
import { Link } from "react-router-dom";
import formatDate from "../helpers/DateTimeUtils";

const TableUserRow = ({
  user: {
    name: { title, first, last },
    picture: { large },
    email,
    dob: { date },
    login: { uuid },
  },
}) => {
  return (
    <tr className="user-row">
      <td>{`${title} ${first} ${last}`}</td>
      <td>
        <div className="image-div">
          <img src={large} alt="user portrait" />
        </div>
      </td>
      <td>{email}</td>
      <td>{formatDate(date)}</td>
      <td>
        <Link className="btn btn--small" to={`/user/${uuid}`}>
          {`More info about ${first}!`}
        </Link>
      </td>
    </tr>
  );
};

export default TableUserRow;
