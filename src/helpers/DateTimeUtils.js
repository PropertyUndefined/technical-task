const formatDate = (dateString) => {
  const date = new Date(dateString);
  const result = `${date.getDate()}/${
    date.getMonth() + 1
  }/${date.getFullYear()}`;
  return result;
};

export default formatDate;
