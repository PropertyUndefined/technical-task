export const ascendingSurnameSort = (userOne, userTwo, sortIsAscending) => {
  if (userOne.name.last.toLowerCase() < userTwo.name.last.toLowerCase()) {
    return sortIsAscending ? -1 : 1;
  } else if (
    userOne.name.last.toLowerCase() >= userTwo.name.last.toLowerCase()
  ) {
    return sortIsAscending ? 1 : -1;
  }
};

export const ascendingAgeSort = (userOne, userTwo, sortIsAscending) => {
  const result = userOne.dob.age - userTwo.dob.age;
  return sortIsAscending ? result : result * -1;
};
